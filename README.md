# Transaction History Retrieval Script

This script is used to retrieve transaction history for a specific smart contract and wallet address using the Polygon API. The transaction data is then parsed and saved to a CSV file.

## Requirements
- Node.js
- Axios (`npm install axios`)
- json2csv (`npm install json2csv`)
- fs (built-in Node.js library)

## Setup
1. Replace Nft address with the address of the smart contract you want to retrieve the transaction history for.
2. Replace contract address with the wallet address you want to retrieve the transaction history for.
3. Replace polygon api with your valid API key from the polygon.
4. Replace https://api-testnet.polygonscan.com/api?module=account&action=txlist&address=//{contract address}&startblock=0&endblock=99999999&page=1offset=10sort=ascapikey=YourApiKeyToken1 with the appropriate URL for the polygon API endpoint you want to call.
5. Run the script using node scriptname.js

## Output
The script will save the transaction data to a file called transactions.csv in the same directory as the script. The script will also print "File saved successfully!" if the file is written, otherwise it will print an error message.

## Note
Please check the documentation of polygon to find the right endpoint to use and construct the URL.
